﻿using System;

namespace BookSharing.Domain.Common
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
    }
}

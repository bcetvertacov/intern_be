﻿using System.ComponentModel.DataAnnotations;

namespace BookSharing.Domain.Common
{
    public abstract class ImageBase : EntityBase
    {
        public byte[] Image { get; set; }
    }
}

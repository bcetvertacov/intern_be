﻿namespace BookSharing.Domain.Enums
{
    public enum Language
    {
        /// <summary>
        /// Undefined
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// English
        /// </summary>
        English = 1,

        /// <summary>
        /// Romanian
        /// </summary>
        Romanian = 2,

        /// <summary>
        /// Russian
        /// </summary>
        Russian = 3
    }
}

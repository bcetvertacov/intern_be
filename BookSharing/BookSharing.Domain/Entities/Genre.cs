﻿using System.Collections.Generic;
using BookSharing.Domain.Common;

namespace BookSharing.Domain.Entities
{
    public class Genre : EntityBase
    {
        public string Name { get; set; }

        public virtual ICollection<Book> Books { get; private set; } = new List<Book>();
    }
}

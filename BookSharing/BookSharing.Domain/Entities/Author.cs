﻿using System.Collections.Generic;
using BookSharing.Domain.Common;

namespace BookSharing.Domain.Entities
{
    public class Author : EntityBase
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsDraft { get; set; }

        public string AddedByUserId { get; set; }

        public virtual ICollection<Book> Books { get; private set; } = new List<Book>();
    }
}

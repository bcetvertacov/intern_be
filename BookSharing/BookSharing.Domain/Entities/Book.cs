﻿using System;
using System.Collections.Generic;
using BookSharing.Domain.Common;
using BookSharing.Domain.Enums;

namespace BookSharing.Domain.Entities
{
    public class Book : EntityBase
    {
        public BookCoverImage CoverImage { get; set; }

        public Guid? CoverImageId { get; set; }

        public string Title { get; set; }

        public virtual ICollection<Author> Authors { get; private set; } = new List<Author>();

        public Language Language { get; set; }

        public virtual ICollection<Genre> Genres { get; private set; } = new List<Genre>();

        public string OwnerId { get; set; }

        public DateTime PublicationDate { get; set; }
    }
}

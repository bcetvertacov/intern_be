﻿using System;
using AutoMapper;
using BookSharing.Application.Common.Models.Dtos;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Infrastructure.Identity;
using BookSharing.Infrastructure.Identity.Models;

namespace BookSharing.Infrastructure.Configurations
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserViewModelIn, User>()
                .ForMember(user => user.UserName, opt => opt.MapFrom(vm => vm.Username));

            CreateMap<User, UserDto>().ReverseMap();
        }
    }
}

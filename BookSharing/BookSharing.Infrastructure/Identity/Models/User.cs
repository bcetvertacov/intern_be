﻿using System;
using BookSharing.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace BookSharing.Infrastructure.Identity.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ProfileImage ProfileImage { get; set; }
    }
}

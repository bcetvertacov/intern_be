﻿using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Infrastructure.Identity.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Infrastructure.Persistence.Seed
{
    public static class InitialDataSeeder
    {
        public static async Task SeedIdentityDataAsync(this IApplicationBuilder builder)
        {
            Guard.Against.Null(builder, nameof(builder));

            using (var serviceScope = builder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var services = serviceScope.ServiceProvider;

                var userManager = services.GetRequiredService<UserManager<User>>();
                var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

                var dbContext = services.GetRequiredService<BookSharingDbContext>();

                await BookSharingDbContextDataSeed.SeedAsync(userManager, roleManager, dbContext);
            }
        }
    }
}

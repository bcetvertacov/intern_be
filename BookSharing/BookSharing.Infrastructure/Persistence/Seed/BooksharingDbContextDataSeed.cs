﻿using System.Linq;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Domain.Entities;
using BookSharing.Infrastructure.Identity.Models;
using Microsoft.AspNetCore.Identity;

namespace BookSharing.Infrastructure.Persistence.Seed
{
    public static class BookSharingDbContextDataSeed
    {
        public static async Task SeedAsync(
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            BookSharingDbContext dbContext)
        {
            Guard.Against.Null(roleManager, nameof(roleManager));
            Guard.Against.Null(userManager, nameof(userManager));
            Guard.Against.Null(dbContext, nameof(dbContext));

            if (await roleManager.FindByNameAsync("Administrator") is null)
            {
                await roleManager.CreateAsync(new IdentityRole("Administrator"));
            }

            if (await roleManager.FindByNameAsync("User") is null)
            {
                await roleManager.CreateAsync(new IdentityRole("User"));
            }

            if (await userManager.FindByNameAsync("superadmin") is null)
            {
                var adminUser = new User
                {
                    UserName = "superadmin",
                    Email = "super@dmin.com",
                    FirstName = "BookSharing",
                    LastName = "SuperAdmin",
                };

                await userManager.CreateAsync(adminUser, "superadmin");
                await userManager.AddToRoleAsync(adminUser, "Administrator");
            }

            if (!dbContext.Genres.Any())
            {
                await dbContext.Genres.AddRangeAsync(
                    new Genre { Name = "Technical Literature" },
                    new Genre { Name = "Detective And Mystery" },
                    new Genre { Name = "Fantasy And Science Fiction" },
                    new Genre { Name = "Thrillers And Horror" },
                    new Genre { Name = "Childrens Fiction" },
                    new Genre { Name = "Inspirational And Self-help" },
                    new Genre { Name = "Biography And Memoir" },
                    new Genre { Name = "Action And Adventure" },
                    new Genre { Name = "Cookbooks" },
                    new Genre { Name = "Poetry" },
                    new Genre { Name = "Other" });

                await dbContext.SaveChangesAsync();
            }

            if (!dbContext.Authors.Any(a => a.IsDraft == false))
            {
                await dbContext.AddRangeAsync(
                    new Author { FirstName = "Ion", LastName = "Creanga", IsDraft = false },
                    new Author { FirstName = "Mihai", LastName = "Eminemscu", IsDraft = false },
                    new Author { FirstName = "Vasile", LastName = "Alecsandri", IsDraft = false },
                    new Author { FirstName = "George", LastName = "Cosbuc", IsDraft = false });

                await dbContext.SaveChangesAsync();
            }
        }
    }
}

﻿using Ardalis.GuardClauses;
using BookSharing.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSharing.Infrastructure.Persistence.Configurations
{
    public class BookCoverImageConfiguration : IEntityTypeConfiguration<BookCoverImage>
    {
        public void Configure(EntityTypeBuilder<BookCoverImage> builder)
        {
            Guard.Against.Null(builder, nameof(builder));

            builder.Property(e => e.Image).IsRequired();
        }
    }
}

﻿using Ardalis.GuardClauses;
using BookSharing.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSharing.Infrastructure.Persistence.Configurations
{
    public class ProfileImageConfiguration : IEntityTypeConfiguration<ProfileImage>
    {
        public void Configure(EntityTypeBuilder<ProfileImage> builder)
        {
            Guard.Against.Null(builder, nameof(builder));

            // builder.Property(e => e.Image).IsRequired();

            // Commented because: Image should not be required.
            // There is a 1-1 relationship between user and profileImage.
            // When the user is created, an instance of profileImage is created as well
            // for that user.
            // Initially, the profileImage instance has the Image property as null (there is no image yet).
            // When he sets a profile image, then only the profileImage.Image property gets updated.
        }
    }
}

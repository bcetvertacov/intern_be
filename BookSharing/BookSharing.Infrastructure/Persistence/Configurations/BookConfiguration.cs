﻿using Ardalis.GuardClauses;
using BookSharing.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSharing.Infrastructure.Persitance.Configurations
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            Guard.Against.Null(builder, nameof(builder));

            builder.Property(e => e.Title).IsRequired().HasMaxLength(300);

            builder.HasMany(e => e.Authors)
                .WithMany(e => e.Books)
                .UsingEntity(join => join.ToTable("BookAuthors"));

            builder
                .HasMany(e => e.Genres)
                .WithMany(e => e.Books)
                .UsingEntity(join => join.ToTable("BookGenres"));
        }
    }
}

﻿using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Domain.Entities;
using BookSharing.Infrastructure.Identity;
using BookSharing.Infrastructure.Identity.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Infrastructure.Persistence
{
    public class BookSharingDbContext : IdentityDbContext<User>, IBookSharingDbContext
    {
        public BookSharingDbContext(DbContextOptions<BookSharingDbContext> options)
            : base(options)
        {
        }

        protected BookSharingDbContext()
        {
        }

        public DbSet<BookCoverImage> BookCoverImages { get; set; }

        public DbSet<ProfileImage> ProfileImages { get; set; }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Genre> Genres { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Guard.Against.Null(builder, nameof(builder));

            builder.ApplyConfigurationsFromAssembly(typeof(BookSharingDbContext).Assembly);

            base.OnModelCreating(builder);
        }
    }
}

﻿using System;
using Ardalis.GuardClauses;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookSharing.Infrastructure.Migrations
{
    /// <summary>
    /// Image column accepts NULL.
    /// </summary>
    public partial class AllowNullOnProfileImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            Guard.Against.Null(migrationBuilder, nameof(migrationBuilder));

            migrationBuilder.AlterColumn<byte[]>(
                name: "Image",
                table: "ProfileImages",
                type: "varbinary(max)",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            Guard.Against.Null(migrationBuilder, nameof(migrationBuilder));

            migrationBuilder.AlterColumn<byte[]>(
                name: "Image",
                table: "ProfileImages",
                type: "varbinary(max)",
                nullable: false,
                defaultValue: Array.Empty<byte>(),
                oldClrType: typeof(byte[]),
                oldType: "varbinary(max)",
                oldNullable: true);
        }
    }
}

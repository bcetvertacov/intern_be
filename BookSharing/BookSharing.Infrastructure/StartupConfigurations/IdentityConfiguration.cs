﻿using System;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Infrastructure.Identity.Models;
using BookSharing.Infrastructure.Persistence;
using BookSharing.Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Infrastructure.StartupConfigurations
{
    public static class IdentityConfiguration
    {
        public static IServiceCollection ConfigureIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            Guard.Against.Null(configuration, nameof(configuration));

            services.AddIdentity<User, IdentityRole>()
                .AddUserManager<UserManager<User>>()
                .AddEntityFrameworkStores<BookSharingDbContext>()
                .AddSignInManager<SignInManager<User>>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 3;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<ITokenService, TokenService>();

            return services;
        }
    }
}

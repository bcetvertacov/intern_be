﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Infrastructure.StartupConfigurations
{
    public static class AutoMapperConfiguration
    {
        public static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
        {
            // Automapper is being injected by Infrastructure,
            // But mapping profiles can be found in both Infrastructure and Application.
            services.AddAutoMapper(
                Assembly.GetExecutingAssembly(),
                Assembly.Load("BookSharing.Application"));

            return services;
        }
    }
}

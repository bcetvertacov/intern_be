﻿using System;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Infrastructure.StartupConfigurations
{
    public static class DbContextConfiguration
    {
        public static IServiceCollection ConfigureDbContext(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            Guard.Against.Null(configuration, nameof(configuration));

            if (bool.Parse(configuration.GetSection("Database:UseInMemory").Value))
            {
                services.AddDbContext<BookSharingDbContext>(options =>
                    options.UseInMemoryDatabase("BookSharingDb"));
            }
            else
            {
                services.AddDbContext<BookSharingDbContext>(options =>
                {
                    options.UseSqlServer(
                        configuration.GetConnectionString("DefaultConnection"),
                        opt => opt.MigrationsAssembly(typeof(BookSharingDbContext).Assembly.FullName));
                });
            }

            services.AddScoped<IBookSharingDbContext>(provider => provider.GetService<BookSharingDbContext>());

            return services;
        }
    }
}

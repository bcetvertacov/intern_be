﻿using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Dtos;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Domain.Entities;
using BookSharing.Infrastructure.Identity.Models;
using BookSharing.Infrastructure.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Infrastructure.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly BookSharingDbContext _dbContext;

        public IdentityService(
            UserManager<User> userManager,
            IMapper mapper,
            BookSharingDbContext dbContext)
        {
            _userManager = userManager;
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<string> CreateUserAsync(UserViewModelIn user)
        {
            Guard.Against.Null(user, nameof(user));

            User newUser = _mapper.Map<User>(user);
            newUser.ProfileImage = new ProfileImage();

            var result = await _userManager.CreateAsync(newUser, user.Password);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(newUser, "User");
            }

            return result.Succeeded ? newUser.Id : null;
        }

        public async Task<UserDto> GetUserByIdAsync(string userId)
        {
            Guard.Against.Null(userId, nameof(userId));

            var user = await _dbContext.Users
                .Include(u => u.ProfileImage)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user is null)
            {
                throw new Application.Common.Exceptions.NotFoundException(nameof(User), userId);
            }

            return _mapper.Map<UserDto>(user);
        }

        public async Task<bool> IsUniqueEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email) == null;
        }

        public async Task<bool> IsUniqueUsernameAsync(string username)
        {
            return await _userManager.FindByNameAsync(username) == null;
        }

        public async Task<bool> SetProfileImage(string userId, byte[] image)
        {
            Guard.Against.Null(userId, nameof(userId));
            Guard.Against.Null(image, nameof(image));

            var user = await _dbContext.Users
                .Include(u => u.ProfileImage)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user is null)
            {
                throw new Application.Common.Exceptions.NotFoundException(nameof(User), userId);
            }

            // If somehow user's ProfileImage was deleted
            if (user.ProfileImage == null)
            {
                user.ProfileImage = new ProfileImage();
                await _dbContext.SaveChangesAsync();
            }

            user.ProfileImage.Image = image;

            _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}

﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Exceptions;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Authentication;
using BookSharing.Infrastructure.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BookSharing.Infrastructure.Services
{
    public class TokenService : ITokenService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly Token _token;

        public TokenService(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IOptions<Token> tokenOptions)
        {
            Guard.Against.Null(tokenOptions, nameof(tokenOptions));

            _token = tokenOptions.Value;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<TokenResponse> Authenticate(TokenRequest request)
        {
            Guard.Against.Null(request, nameof(request));

            var user = await _userManager.FindByNameAsync(request.Username);

            // Invalid Username
            if (user == null)
            {
                throw new InvalidCredentialsException();
            }

            bool isValidCredentials = await IsValidCredentials(user, request.Password);

            // Invalid Password
            if (!isValidCredentials)
            {
                throw new InvalidCredentialsException();
            }

            string role = (await _userManager.GetRolesAsync(user))[0];
            string jwtToken = GenerateJwtToken(user, role);

            return new TokenResponse(user.Id, user.Email, role, jwtToken);
        }

        private async Task<bool> IsValidCredentials(User user, string password)
        {
            return (await _signInManager.PasswordSignInAsync(user, password, true, false))
                .Succeeded;
        }

        private string GenerateJwtToken(User user, string role)
        {
            byte[] secret = Encoding.ASCII.GetBytes(_token.Secret);

            var descriptor = new SecurityTokenDescriptor
            {
                Issuer = _token.Issuer,
                Audience = _token.Audience,
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", user.Id),
                    new Claim("FirstName", user.FirstName),
                    new Claim("LastName", user.LastName),
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.NameIdentifier, user.UserName),
                    new Claim(ClaimTypes.Role, role)
                }),
                Expires = DateTime.UtcNow.AddMinutes(_token.ExpirationTime),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(secret),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var handler = new JwtSecurityTokenHandler();
            SecurityToken token = handler.CreateToken(descriptor);

            return handler.WriteToken(token);
        }
    }
}

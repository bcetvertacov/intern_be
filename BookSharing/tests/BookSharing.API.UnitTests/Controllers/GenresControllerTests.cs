﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BookSharing.API.Controllers;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Application.CQRS.GenreActions.Queries;
using Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace BookSharing.API.UnitTests.Controllers
{
    public class GenresControllerTests : IDisposable
    {
        private readonly Mock<IMediator> _mediatorMock;

        public GenresControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
        }

        public void Dispose()
        {
            _mediatorMock.VerifyAll();
        }

        [Theory]
        [AutoMoqData]
        public async Task  GetAllGenres_Success(           
            QueryOptions queryOptions,
            QueryResult<GenreViewModelOut> queryResult)
        {            
            _mediatorMock
                .Setup(x => x.Send(It.IsAny<GetAllGenresQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(queryResult);

            var sut = new GenresController(_mediatorMock.Object);

            var result = await sut.GetAllGenres(queryOptions);

            Assert.IsType<OkObjectResult>(result);

            Assert.NotNull(result);
        }
    }
}
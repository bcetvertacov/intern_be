﻿using AutoFixture;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Common.Customizations
{
    public class BookSharingDbContextCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {

            fixture.Customize<Mock<IBookSharingDbContext>>(composer =>
                composer.Do(dbContextMock =>
                {
                    dbContextMock.Setup(m => m.Books)
                    .Returns(GetFakeDbSet<Book>());

                    dbContextMock.Setup(m => m.Authors)
                        .Returns(GetFakeDbSet<Author>());

                    dbContextMock.Setup(m => m.BookCoverImages)
                        .Returns(GetFakeDbSet<BookCoverImage>());

                    dbContextMock.Setup(m => m.ProfileImages)
                        .Returns(GetFakeDbSet<ProfileImage>());

                    dbContextMock.Setup(m => m.ProfileImages)
                        .Returns(GetFakeDbSet<ProfileImage>());

                    dbContextMock.Setup(m => m.Genres)
                        .Returns(GetFakeDbSet<Genre>());
                }));
        }

        private DbSet<T> GetFakeDbSet<T>()
            where T : class
        {
            var entities = new List<T>().AsQueryable();
            var mock = entities.BuildMockDbSet();

            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(entities.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(entities.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(entities.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(entities.GetEnumerator());

            return mock.Object;
        }
    }
}

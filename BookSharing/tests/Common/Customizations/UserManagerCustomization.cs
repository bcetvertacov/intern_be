﻿using AutoFixture;
using BookSharing.Infrastructure.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace Common.Customizations
{
    public class UserManagerCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            // Not Great, Not Terrible
            // TODO: Find a way to configure both UserManager<User> and Mock<UserManager<User>> via
            // a single register / inject / customize.
            
            var mgr = new Mock<UserManager<User>>(Mock.Of<IUserRoleStore<User>>(),
                   null, null, null, null, null, null, null, null);

            mgr.Setup(x => x.DeleteAsync(It.IsAny<User>()))
                    .ReturnsAsync(IdentityResult.Success);

            mgr.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            mgr.Setup(x => x.UpdateAsync(It.IsAny<User>()))
                .ReturnsAsync(IdentityResult.Success);

            mgr.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()))
               .ReturnsAsync(IdentityResult.Success);

            fixture.Customize<UserManager<User>>(composer =>
                composer.FromFactory(() =>
                {
                    return mgr.Object;
                }));

            fixture.Customize<Mock<UserManager<User>>>(composer =>
                composer.FromFactory(() =>
                {
                    return mgr;
                }));
        }
    }
}

﻿using System.Reflection;
using AutoFixture;
using AutoMapper;

namespace Common.Customizations
{
    public class MapperCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            // Reference - https://stackoverflow.com/a/58892231/9517443
            // The whole point of AutoMapper is to not write a lot of mapping code.
            // So, as a result of that, the default mock that is passed by autofixture
            // is a real instance of AutoMapper.

            fixture.Register<IMapper>(() =>
            {
                var configuration = new MapperConfiguration(cfg =>
                {
                    cfg.AddMaps(
                        Assembly.Load("BookSharing.Application"),
                        Assembly.Load("BookSharing.Infrastructure"));
                });

                return new Mapper(configuration);
            });
        }
    }
}

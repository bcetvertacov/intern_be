﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using Common.Customizations;

namespace Common
{
    /// <summary>
    /// This helps us to use Moq in tandem with AutoFixture
    /// </summary>
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public static IFixture FixtureFactory()
        {
            var f = new Fixture();

            f.Customize(new AutoMoqCustomization() { ConfigureMembers = true });
            f.Customize(new UserManagerCustomization());
            f.Customize(new BookSharingDbContextCustomization());
            f.Customize(new MapperCustomization());

            return f;
        }

        public AutoMoqDataAttribute() : base(FixtureFactory) { }
    }
}

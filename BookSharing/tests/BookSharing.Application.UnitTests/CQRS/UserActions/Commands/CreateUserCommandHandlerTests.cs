﻿using AutoFixture.Xunit2;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.CQRS.AuthorActions.Commands;
using BookSharing.Application.CQRS.UserActions.Commands.CreateUser;
using BookSharing.Domain.Entities;
using BookSharing.Infrastructure.Services;
using Common;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookSharing.Application.UnitTests.CQRS.UserActions.Commands
{
    public class CreateUserCommandHandlerTests
    {
        [Theory]
        [AutoMoqData]
        public async Task Handle_ShouldCallCreateUserAsync_Once(
            [Frozen] Mock<IIdentityService> identityServiceMock,
            CreateUserCommandHandler handler,
            CreateUserCommand command,
            CancellationToken cancellationToken)
        {
            // Act
            await handler.Handle(command, cancellationToken);

            // Assert
            identityServiceMock.Verify(m => m.CreateUserAsync(command), Times.Once);
        }

        [Theory]
        [AutoMoqData]
        public async Task Handle_ShouldReturnUserIdFromIdentityService(
            [Frozen] Mock<IIdentityService> identityServiceMock,
            CreateUserCommandHandler handler,
            CreateUserCommand command,
            CancellationToken cancellationToken,
            string expectedId)
        {
            // Arrange
            identityServiceMock.Setup(m => m.CreateUserAsync(command))
                .ReturnsAsync(expectedId);

            // Act
            var actualId = await handler.Handle(command, cancellationToken);

            // Assert
            Assert.Equal(expectedId, actualId);
        }
    }
}

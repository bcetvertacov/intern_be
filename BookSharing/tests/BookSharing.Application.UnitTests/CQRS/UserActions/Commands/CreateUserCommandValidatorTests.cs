﻿using AutoFixture.Xunit2;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.CQRS.UserActions.Commands.CreateUser;
using Common;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace BookSharing.Application.UnitTests.CQRS.UserActions.Commands
{
    public class CreateUserCommandValidatorTests
    {
        [Theory]
        [InlineAutoMoqData("")]
        [InlineAutoMoqData("Ab")]
        [InlineAutoMoqData("AaAaaaaaaaaaaaaaaaaaaaaaaaaaaaa")] // 31
        [InlineAutoMoqData("     ")]
        [InlineAutoMoqData("_Johny")]
        [InlineAutoMoqData("Johny1")]
        [InlineAutoMoqData("Johny.")]
        [InlineAutoMoqData("Johny ")]
        public void Create_FirstNameInvalid_ReturnValidationFailure(
            string firstName,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.FirstName = firstName;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.FirstName));
        }

        [Theory]
        [InlineAutoMoqData("Ion")]
        [InlineAutoMoqData("John")]
        [InlineAutoMoqData("Johnyyyyyyyyyyyyyyyyyyyyyyyyy")] // 30
        public void Create_FirstNameValid_NoValidationFailure(
            string firstName,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.FirstName = firstName;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.FirstName));
        }

        [Theory]
        [InlineAutoMoqData("")]
        [InlineAutoMoqData("Ab")]
        [InlineAutoMoqData("AaAaaaaaaaaaaaaaaaaaaaaaaaaaaaa")] // 31
        [InlineAutoMoqData("     ")]
        [InlineAutoMoqData("_Johny")]
        [InlineAutoMoqData("Johny1")]
        [InlineAutoMoqData("Johny.")]
        [InlineAutoMoqData("Johny ")]
        public void Create_LastNameInvalid_ReturnValidationFailure(
            string lastName,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.LastName = lastName;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.FirstName));
        }

        [Theory]
        [InlineAutoMoqData("Ion")]
        [InlineAutoMoqData("John")]
        [InlineAutoMoqData("Johnyyyyyyyyyyyyyyyyyyyyyyyyy")] // 30
        public void Create_LastNameValid_NoValidationFailure(
            string lastName,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.LastName = lastName;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.LastName));
        }

        [Theory]
        [InlineAutoMoqData("")]
        [InlineAutoMoqData("Ab")]
        [InlineAutoMoqData("AaAaaaaaaaaaaaaaaaaaaaaaaaaaaaa")] // 31
        [InlineAutoMoqData("     ")]
        [InlineAutoMoqData("_Johny")]
        [InlineAutoMoqData("Johny.")]
        [InlineAutoMoqData("Johny ")]
        public void Create_UsernameInvalid_ReturnValidationFailure(
            string username,
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Username = username;

            identityService.Setup(i => i.IsUniqueUsernameAsync(username))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.Username));
        }

        [Theory]
        [InlineAutoMoqData("us3")]
        [InlineAutoMoqData("usern4m3")]
        [InlineAutoMoqData("AaAaaaaaaaaaaaaaaaaaaaaaaaaaaa")] // 30
        public void Create_UsernameValid_NoValidationFailure(
            string username,
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Username = username;

            identityService.Setup(i => i.IsUniqueUsernameAsync(username))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.Username));
        }

        [Theory]
        [AutoMoqData]
        public void Create_UsernameExists_ReturnValidationFailure(
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Username = "us3rname";

            identityService.Setup(i => i.IsUniqueUsernameAsync(command.Username))
                .Returns(Task.FromResult(false));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.Username));
        }

        [Theory]
        [AutoMoqData]
        public void Create_UsernameUnique_NoValidationFailure(
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Username = "us3rname";

            identityService.Setup(i => i.IsUniqueUsernameAsync(command.Username))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.Username));
        }

        [Theory]
        [InlineAutoMoqData("")]
        [InlineAutoMoqData("???@.@")]
        [InlineAutoMoqData("???@.com")]
        [InlineAutoMoqData("11111.com")]
        [InlineAutoMoqData("email@dress")]
        [InlineAutoMoqData("test.test@123")]
        [InlineAutoMoqData("test***test@123")]
        [InlineAutoMoqData("alexan@thegreat")]
        [InlineAutoMoqData("test????test@123")]
        [InlineAutoMoqData("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com")] // 71
        public void Create_EmailInvalid_ReturnValidationFailure(
            string email,
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Email = email;

            identityService.Setup(i => i.IsUniqueEmailAsync(email))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.Email));
        }

        [Theory]
        [InlineAutoMoqData("Elliot@mail.ru")]
        [InlineAutoMoqData("AndrewEle@yahoo.com")]
        [InlineAutoMoqData("AndrewEle@gmail.com")]
        [InlineAutoMoqData("AndrewEle@endava.com")]
        public void Create_EmailValid_NoValidationFailure(
            string email,
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Email = email;

            identityService.Setup(i => i.IsUniqueEmailAsync(email))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.Email));
        }

        [Theory]
        [AutoMoqData]
        public void Create_EmailExists_ReturnValidationFailure(
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Email = "test@gmail.com";

            identityService.Setup(i => i.IsUniqueEmailAsync(command.Email))
                .Returns(Task.FromResult(false));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.Email));
        }

        [Theory]
        [AutoMoqData]
        public void Create_EmailUnique_NoValidationFailure(
            [Frozen] Mock<IIdentityService> identityService,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Email = "test@gmail.com";

            identityService.Setup(i => i.IsUniqueEmailAsync(command.Email))
                .Returns(Task.FromResult(true));

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.Email));
        }

        [Theory]
        [InlineAutoMoqData("")]
        [InlineAutoMoqData("Ab")]
        [InlineAutoMoqData("A2A2aaaaaaaaaaaaaaaaaaaaaaaaaaa")] // 31
        [InlineAutoMoqData("-=21")]
        [InlineAutoMoqData("AbPasss02.3")]
        [InlineAutoMoqData("...--2Password")]
        public void Create_PasswordInvalid_ReturnValidationFailure(
            string password,
            CreateUserCommandValidator sut,
            CreateUserCommand command)
        {
            // Arrange
            command.Password = password;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.Contains(
                result.Errors,
                o => o.PropertyName == nameof(command.Password));
        }

        [Theory]
        [InlineAutoMoqData("P4ssw0rd")]
        [InlineAutoMoqData("p3R")]
        [InlineAutoMoqData("A2A2aaaaaaaaaaaaaaaaaaaaaaaaaa")] // 30
        public void Create_PasswordValid_NoValidationFailure(
           string password,
           CreateUserCommandValidator sut,
           CreateUserCommand command)
        {
            // Arrange
            command.Password = password;

            // Act
            var result = sut.Validate(command);

            // Assert
            Assert.DoesNotContain(
                result.Errors,
                o => o.PropertyName == nameof(command.Password));
        }
    }
}

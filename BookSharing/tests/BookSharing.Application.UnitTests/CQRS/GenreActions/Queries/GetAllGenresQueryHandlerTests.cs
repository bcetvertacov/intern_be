﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Application.CQRS.GenreActions.Queries;
using BookSharing.Domain.Entities;
using Common;
using Moq;
using Xunit;

namespace BookSharing.Application.UnitTests.CQRS.GenreActions.Queries
{
    public class GetAllGenresQueryHandlerTests : IDisposable
    {
        private readonly Mock<IBookSharingDbContext> _dbContextMock;

        private readonly Mock<IQueryBuilder> _queryBuilderMock;

        public GetAllGenresQueryHandlerTests()
        {
            _dbContextMock = new Mock<IBookSharingDbContext>();
            _queryBuilderMock = new Mock<IQueryBuilder>();
        }

        public void Dispose()
        {
            _dbContextMock.VerifyAll();
            _queryBuilderMock.VerifyAll();
        }

        [Fact]       
        public async Task Handle_NullObject_ThrowsException()            
        {
            var sut = new GetAllGenresQueryHandler(_dbContextMock.Object, _queryBuilderMock.Object);    
            
            Task result() => sut.Handle(null, CancellationToken.None);

            await Assert.ThrowsAsync<ArgumentNullException>(result);            
        }

        [Theory]
        [AutoMoqData]
        public async Task Handle_Success(
            GetAllGenresQuery query,
            QueryResult<GenreViewModelOut> queryResult)
        {
            _queryBuilderMock
                .Setup(x => x.GetQueryResult<Genre, GenreViewModelOut>(
                    It.IsAny<IQueryable<Genre>>(),
                    It.IsAny<QueryOptions>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(queryResult);

            var sut = new GetAllGenresQueryHandler(_dbContextMock.Object, _queryBuilderMock.Object);

            var result = await sut.Handle(query, CancellationToken.None);

            Assert.NotNull(result);
        }
    }
}
﻿using AutoFixture.Xunit2;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Infrastructure.Identity.Models;
using BookSharing.Infrastructure.Services;
using Common;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookSharing.Infrastructure.UnitTests.Services
{
    public class IdentityServiceTests
    {
        [Theory]
        [AutoMoqData]
        public async Task CreateUser_NullUser_ShouldThrowArgumentNullException(
            IdentityService sut)
        {
            // Act
            var exception = await Record.ExceptionAsync(() => sut.CreateUserAsync(null));

            // Assert
            Assert.True(exception is ArgumentNullException);
        }

        [Theory]
        [AutoMoqData]
        public async Task CreateUser_ValidUser_ShouldCreateValidUserRecord(
            [Frozen] Mock<UserManager<User>> userManagerMock,
            IdentityService sut,
            UserViewModelIn userVm)
        {
            // Act
            await sut.CreateUserAsync(userVm);

            // Assert
            userManagerMock.Verify(m => m
                .CreateAsync(
                    It.Is<User>(u =>
                        UserEqualsUserViewModel(u, userVm)
                        && u.ProfileImage != null),
                    userVm.Password
                ),
                Times.Once);
        }

        [Theory]
        [AutoMoqData]
        public async Task CreateUser_ValidUser_ShouldAddToUserRoleOnSuccessfulCreation(
            [Frozen] Mock<UserManager<User>> userManagerMock,
            IdentityService sut,
            UserViewModelIn userVm)
        {
            // Arrange
            string defaultRole = "User";

            // Act
            await sut.CreateUserAsync(userVm);

            // Assert
            userManagerMock.Verify(m => m
                .AddToRoleAsync(
                    It.Is<User>(u =>
                        UserEqualsUserViewModel(u, userVm)
                        && u.ProfileImage != null),
                    defaultRole),
                Times.Once);
        }

        [Theory]
        [AutoMoqData]
        public async Task CreateUser_ValidUser_ShouldReturnUserIdOnSuccessfulCretion(
            IdentityService sut,
            UserViewModelIn userVm)
        {
            // Act
            var result = await sut.CreateUserAsync(userVm);

            // Assert
            Assert.NotNull(result);
        }

        [Theory]
        [AutoMoqData]
        public async Task CreateUser_InvalidUser_ShouldReturnNull(
            [Frozen] Mock<UserManager<User>> userManagerMock,
            IdentityService sut,
            UserViewModelIn userVm)
        {
            // Arrange
            userManagerMock.Setup(m => m.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed());

            // Act
            var result = await sut.CreateUserAsync(userVm);

            // Assert
            Assert.Null(result);
        }

        [Theory]
        [AutoMoqData]
        public async Task GetUserById_NullId_ShouldThrowArgumentNullException(
            IdentityService sut)
        {
            // Act
            var exception = await Record.ExceptionAsync(()
                => sut.GetUserByIdAsync(null));

            // Assert
            Assert.True(exception is ArgumentNullException);
        }

        private bool UserEqualsUserViewModel(User user, UserViewModelIn userVm)
        {
            return user.UserName == userVm.Username
                && user.FirstName == userVm.FirstName
                && user.LastName == userVm.LastName
                && user.Email == userVm.Email;
        }
    }
}

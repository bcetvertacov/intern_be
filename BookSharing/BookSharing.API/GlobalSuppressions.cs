﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "OK", Scope = "member", Target = "~M:BookSharing.API.Controllers.AuthenticationController.Login(BookSharing.Application.CQRS.AuthActions.Commands.Login.LoginCommand)~System.Threading.Tasks.Task{Microsoft.AspNetCore.Mvc.IActionResult}")]

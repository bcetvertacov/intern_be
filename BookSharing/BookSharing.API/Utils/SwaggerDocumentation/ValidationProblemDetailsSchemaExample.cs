﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace BookSharing.API.Utils.SwaggerDocumentation
{
    public class ValidationProblemDetailsSchemaExample : IExamplesProvider<ValidationProblemDetails>
    {
        public ValidationProblemDetails GetExamples()
        {
            var exampleDetail = new ValidationProblemDetails
            {
                Title = "string",
                Status = 0,
                Detail = "string"
            };
            exampleDetail.Extensions.Add("traceId", "string");
            exampleDetail.Errors.Add(
                "Property",
                new[] { "Error message 1", "Error message 2", "Error message 3" });

            return exampleDetail;
        }
    }
}

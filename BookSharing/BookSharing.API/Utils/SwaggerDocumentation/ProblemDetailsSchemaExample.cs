﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace BookSharing.API.Utils.SwaggerDocumentation
{
    public class ProblemDetailsSchemaExample : IExamplesProvider<ProblemDetails>
    {
        public ProblemDetails GetExamples()
        {
            var exampleDetail = new ProblemDetails
            {
                Title = "string",
                Status = 0,
                Detail = "string"
            };
            exampleDetail.Extensions.Add("traceId", "string");

            return exampleDetail;
        }
    }
}

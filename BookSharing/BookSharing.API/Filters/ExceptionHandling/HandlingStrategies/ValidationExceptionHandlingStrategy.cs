﻿using System;
using System.Linq;
using Ardalis.GuardClauses;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public class ValidationExceptionHandlingStrategy : IExceptionHandlingStrategy
    {
        // Convert ValidationException's collection of 'ValidationFailure's into
        // a dictionary, required to build a ValidationProblemDetails object.
        public void Handle(ExceptionContext context)
        {
            Guard.Against.Null(context, nameof(context));

            var errors = (context.Exception as ValidationException).Errors;

            var errorsDictionary = errors
                .GroupBy(e => e.PropertyName, e => e.ErrorMessage)
                .ToDictionary(group => group.Key, e => e.ToArray());

            var details = new ValidationProblemDetails(errorsDictionary);

            context.Result = new BadRequestObjectResult(details);

            context.ExceptionHandled = true;
        }
    }
}

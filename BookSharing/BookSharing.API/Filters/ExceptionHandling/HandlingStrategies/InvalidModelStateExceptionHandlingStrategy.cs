﻿using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public class InvalidModelStateExceptionHandlingStrategy : IExceptionHandlingStrategy
    {
        public void Handle(ExceptionContext context)
        {
            Guard.Against.Null(context, nameof(context));

            var details = new ValidationProblemDetails(context.ModelState);

            context.Result = new BadRequestObjectResult(details);

            context.ExceptionHandled = true;
        }
    }
}

﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public interface IExceptionHandlingStrategy
    {
        public void Handle(ExceptionContext context);
    }
}

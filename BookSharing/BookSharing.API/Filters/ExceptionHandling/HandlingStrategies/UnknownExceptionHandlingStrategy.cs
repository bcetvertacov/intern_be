﻿using System;
using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public class UnknownExceptionHandlingStrategy : IExceptionHandlingStrategy
    {
        public void Handle(ExceptionContext context)
        {
            Guard.Against.Null(context, nameof(context));

            var details = new ProblemDetails
            {
                Title = "An error occurred while processing your request.",
                Status = StatusCodes.Status500InternalServerError,
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };

            context.ExceptionHandled = true;
        }
    }
}

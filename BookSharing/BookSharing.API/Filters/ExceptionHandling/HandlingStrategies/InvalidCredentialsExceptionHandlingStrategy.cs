﻿using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public class InvalidCredentialsExceptionHandlingStrategy : IExceptionHandlingStrategy
    {
        public void Handle(ExceptionContext context)
        {
            Guard.Against.Null(context, nameof(context));

            var details = new ProblemDetails()
            {
                Title = "Invalid username or password",
                Status = StatusCodes.Status401Unauthorized,
            };

            context.Result = new UnauthorizedObjectResult(details);

            context.ExceptionHandled = true;
        }
    }
}

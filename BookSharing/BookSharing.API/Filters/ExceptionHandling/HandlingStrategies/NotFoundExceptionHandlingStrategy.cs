﻿using System;
using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling.HandlingStrategies
{
    public class NotFoundExceptionHandlingStrategy : IExceptionHandlingStrategy
    {
        public void Handle(ExceptionContext context)
        {
            Guard.Against.Null(context, nameof(context));

            var details = new ProblemDetails()
            {
                Title = "The specified resource was not found.",
                Status = StatusCodes.Status404NotFound,
                Detail = context.Exception.Message
            };

            context.Result = new NotFoundObjectResult(details);

            context.ExceptionHandled = true;
        }
    }
}

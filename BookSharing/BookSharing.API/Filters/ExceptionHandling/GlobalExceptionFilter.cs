﻿using System;
using System.Collections.Generic;
using BookSharing.API.Filters.ExceptionHandling.HandlingStrategies;
using BookSharing.Application.Common.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BookSharing.API.Filters.ExceptionHandling
{
    public sealed class GlobalExceptionFilter : ExceptionFilterAttribute
    {
        // How to add an exception type and to treat it globally:
        //
        // 1) Create your custom exception type
        // 2) Add the handling logic by creating a new ExceptionHandlingStrategy
        //    (it should implement IExeptionHandlingStrategy).
        // 3) Register the exception - handlingStrategy association in _handlers
        //
        // Note: If your exception does not require any specific handling logic,
        // there is no need to create a separate ExceptionHandlingStrategy or to
        // register it within _handlers.
        // The UnknownExceptionHandlingStrategy is applied by default.

        // Dictionary<ExceptionType, StrategyType>
        private readonly Dictionary<Type, Type> _handlers;

        public GlobalExceptionFilter()
        {
            _handlers = new ()
            {
                {
                    typeof(NotFoundException),
                    typeof(NotFoundExceptionHandlingStrategy)
                },
                {
                    typeof(ValidationException),
                    typeof(ValidationExceptionHandlingStrategy)
                },
                {
                    typeof(InvalidCredentialsException),
                    typeof(InvalidCredentialsExceptionHandlingStrategy)
                },
            };
        }

        public override void OnException(ExceptionContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            Type strategyType;

            if (!context.ModelState.IsValid)
            {
                strategyType = typeof(InvalidModelStateExceptionHandlingStrategy);
            }
            else
            {
                strategyType = _handlers.GetValueOrDefault(context.Exception.GetType());

                if (strategyType is null)
                {
                    strategyType = typeof(UnknownExceptionHandlingStrategy);
                }
            }

            (Activator.CreateInstance(strategyType) as IExceptionHandlingStrategy)
                .Handle(context);

            base.OnException(context);
        }
    }
}

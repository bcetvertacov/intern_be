﻿using Ardalis.GuardClauses;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.API.StartupConfigurations
{
    public static class CORSConfiguration
    {
        public static void ConfigureCORS(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            Guard.Against.Null(configuration, nameof(configuration));

            var origins = configuration.GetSection("CORSOrigins")
                        .Get<string[]>();

            if (origins != null && origins.Length > 0)
            {
                services.AddCors(options =>
                {
                    options.AddPolicy("BookSharingPolicy", builder =>
                    {
                        builder.WithOrigins(origins)
                        .AllowCredentials()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    });
                });
            }
        }
    }
}

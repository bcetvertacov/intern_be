﻿using System;
using System.Security.Claims;
using System.Text;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Models.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace BookSharing.API.StartupConfigurations
{
    public static class AuthenticationConfiguration
    {
        public static IServiceCollection ConfigureAuthentication(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            Guard.Against.Null(configuration, nameof(configuration));

            services.Configure<Token>(configuration.GetSection("Token"));

            Token token = configuration.GetSection("Token").Get<Token>();
            byte[] secret = Encoding.ASCII.GetBytes(token.Secret);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.ClaimsIssuer = token.Issuer;
                    options.Audience = token.Audience;
                    options.IncludeErrorDetails = true;
                    options.Validate(JwtBearerDefaults.AuthenticationScheme);
                    options.TokenValidationParameters =
                        new TokenValidationParameters
                        {
                            ClockSkew = TimeSpan.Zero,
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidIssuer = token.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(secret),
                            NameClaimType = ClaimTypes.NameIdentifier,
                        };
                });

            return services;
        }
    }
}

﻿using System.Security.Claims;
using BookSharing.Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace BookSharing.API.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetUserId()
        {
            return _httpContextAccessor.HttpContext?.User?
                .FindFirstValue("UserId");
        }
    }
}

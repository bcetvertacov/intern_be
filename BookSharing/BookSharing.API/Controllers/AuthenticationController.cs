﻿using System.Threading.Tasks;
using BookSharing.Application.CQRS.AuthActions.Commands.Login;
using BookSharing.Application.CQRS.UserActions.Commands.CreateUser;
using Microsoft.AspNetCore.Mvc;

namespace BookSharing.API.Controllers
{
    public class AuthenticationController : ApiBaseController
    {
        [HttpPost("register")]
        public async Task<IActionResult> Register(CreateUserCommand command)
        {
            // CreateUserCommand contains all of the data that we need in order to create an user.
            // All we have to do is to send the CreateUserCommand to Mediator.

            // The mediator will automatically find the coresponding CommandHandler
            var userId = await Mediator.Send(command);

            // Uncomment this after implementing UserController & GetById action
            // return CreatedAtRoute("GetUserById", new { id = userId }, null);
            return Ok(userId);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginCommand command)
        {
            var response = await Mediator.Send(command);

            return Created(response.Id, response);
        }
    }
}

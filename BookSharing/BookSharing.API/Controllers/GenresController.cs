﻿using System.Threading.Tasks;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Application.CQRS.GenreActions.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookSharing.API.Controllers
{
    [Authorize]
    public class GenresController : ApiBaseController
    {
        private readonly IMediator _mediator;

        public GenresController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllGenres([FromQuery] QueryOptions queryOptions)
        {
            var query = new GetAllGenresQuery() { QueryOptions = queryOptions };
            var genres = await _mediator.Send(query);
            return Ok(genres);
        }
    }
}

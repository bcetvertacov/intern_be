﻿using System.Threading.Tasks;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Application.CQRS.UserActions.Commands.UpdateUser;
using BookSharing.Application.CQRS.UserActions.Queries.GetUser;
using BookSharing.Application.CQRS.UserActions.Queries.GetUserBooks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookSharing.API.Controllers
{
    public class UserController : ApiBaseController
    {
        // BIG TODO: Separate UserController into UserController and AccountController
        [HttpGet("profile")]
        [Authorize]
        public async Task<IActionResult> GetProfileInfo()
        {
            var query = new GetUserQuery();

            var result = await Mediator.Send(query);

            return Ok(result);
        }

        [HttpPut("profileImage")]
        [Authorize]
        public async Task<IActionResult> UpdateProfileImage([FromForm] ProfileImageViewModelIn image)
        {
            var command = new UpdateUserCommand() { ProfileImage = image };
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpGet("{id}/books")]
        public async Task<IActionResult> GetUserBooks(string id, [FromQuery] QueryOptions queryOptions)
        {
            var query = new GetUserBooksQuery()
            {
                UserId = id,
                QueryOptions = queryOptions
            };

            var result = await Mediator.Send(query);

            return Ok(result);
        }
    }
}

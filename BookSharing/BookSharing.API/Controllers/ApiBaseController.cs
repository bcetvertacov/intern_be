﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ApiBaseController : ControllerBase
    {
        private IMediator _mediator;

        // Get the mediator instance that is being injected by the Application Layer.
        // It will be the controllers' only dependency

        // The mediator is configured in Application Layer.
        // There are all of the Commands and Queries.
        public IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}

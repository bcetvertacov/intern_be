﻿using System;
using System.Threading.Tasks;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Application.CQRS.BookActions.Commands.CreateBook;
using BookSharing.Application.CQRS.BookActions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BookSharing.API.Controllers
{
    [Authorize]
    public class BooksController : ApiBaseController
    {
        [HttpGet]
        [SwaggerOperation(
            Summary = "Gets all the books that don't have any draft author.",
            Description = "Gets all the books that don't have any draft author.")]
        [SwaggerResponse(
            StatusCodes.Status200OK,
            "Returns all the books that don't have any draft author",
            typeof(BookViewModelOut))]
        public async Task<IActionResult> GetAllBooks([FromQuery]QueryOptions queryOptions)
        {
            var query = new GetAllBooksQuery() { QueryOptions = queryOptions };
            var books = await Mediator.Send(query);
            return Ok(books);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateBook([FromForm] BookViewModelIn bookViewModel)
        {
            var command = new CreateBookCommand()
            {
                Book = bookViewModel
            };

            var bookId = await Mediator.Send(command);

            return Created(new Uri($"{Request.Path}/{bookId}", UriKind.Relative), bookId);
        }
    }
}

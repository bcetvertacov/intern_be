﻿using System;
using System.Threading.Tasks;
using BookSharing.Application.CQRS.AuthorActions.Commands;
using BookSharing.Application.CQRS.AuthorActions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookSharing.API.Controllers
{
    [Authorize]
    public class AuthorsController : ApiBaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetAllAuthors()
        {
            var query = new GetAllAuthorsQuery();
            var allAuthors = await Mediator.Send(query);

            return Ok(allAuthors);
        }

        [HttpGet("{id}", Name = "GetAuthorById")]
        public async Task<IActionResult> GetAuthorById(Guid id)
        {
            var query = new GetAuthorByIdQuery { Id = id };
            var author = await Mediator.Send(query);

            return Ok(author);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateAuthor(CreateAuthorCommand command)
        {
            var authorId = await Mediator.Send(command);
            return CreatedAtRoute("GetAuthorById", new { id = authorId }, null);
        }
    }
}

using BookSharing.API.Filters.ExceptionHandling;
using BookSharing.API.ModelBinders;
using BookSharing.API.Services;
using BookSharing.API.StartupConfigurations;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.StartupConfiguration;
using BookSharing.Infrastructure.Persistence.Seed;
using BookSharing.Infrastructure.StartupConfigurations;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BookSharing.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // BookSharing.Infrastructure services
            services
                .ConfigureDbContext(Configuration)
                .ConfigureIdentity(Configuration);

            // BookSharing.Application services
            services
                .ConfigureMediator()
                .ConfigureFluentValidation()
                .ConfigureAutomapper()
                .ConfigureUtils();

            // BookSharing.Api services
            services.AddControllers(opt =>
            {
                opt.Filters.Add<GlobalExceptionFilter>();
                opt.ModelBinderProviders.Add(new FilteredQueryBinderProvider());
            })
            .AddFluentValidation();

            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.ConfigureAuthentication(Configuration);
            services.ConfigureSwagger();
            services.ConfigureCORS(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BookSharing.API v1"));
            }

            app.UseCors(c => c
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.SeedIdentityDataAsync().Wait();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

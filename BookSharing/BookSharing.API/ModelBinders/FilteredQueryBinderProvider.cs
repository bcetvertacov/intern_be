﻿using System;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Utils.Query;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace BookSharing.API.ModelBinders
{
    public class FilteredQueryBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            Guard.Against.Null(context, nameof(context));

            if (context.Metadata.ModelType == typeof(SortOptions))
            {
                return new BinderTypeModelBinder(typeof(FilteredQueryBinder));
            }

            return null;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BookSharing.Application.Common.Utils.Query
{
    public class FilteredQueryBinder : IModelBinder
    {
        private const string OrderSeparator = ":";

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                return Task.CompletedTask;
            }

            var value = bindingContext.ValueProvider.GetValue(bindingContext.FieldName).FirstValue;
            if (string.IsNullOrWhiteSpace(value))
            {
                return Task.CompletedTask;
            }

            var sortParts = value.Split(OrderSeparator, StringSplitOptions.RemoveEmptyEntries);
            var fieldName = sortParts[0];
            var direction = sortParts.Length == 2 ? ResolveSortDirection(sortParts[1]) : SortDirection.Ascending;

            bindingContext.Result = ModelBindingResult.Success(
                new SortOptions { FieldName = fieldName, SortDirection = direction });

            return Task.CompletedTask;
        }

        public SortDirection ResolveSortDirection(string direction)
        {
            return direction == "asc" ? SortDirection.Ascending : SortDirection.Descending;
        }
    }
}
﻿namespace BookSharing.Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        public string GetUserId();
    }
}

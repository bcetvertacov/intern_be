﻿using System.Threading.Tasks;
using BookSharing.Application.Common.Models.Dtos;
using BookSharing.Application.Common.Models.ViewModels;

namespace BookSharing.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<string> CreateUserAsync(UserViewModelIn user);

        Task<UserDto> GetUserByIdAsync(string userId);

        Task<bool> SetProfileImage(string userId, byte[] image);

        Task<bool> IsUniqueEmailAsync(string email);

        Task<bool> IsUniqueUsernameAsync(string username);
    }
}

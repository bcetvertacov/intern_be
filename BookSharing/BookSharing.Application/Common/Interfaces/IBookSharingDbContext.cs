﻿using System.Threading;
using System.Threading.Tasks;
using BookSharing.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.Common.Interfaces
{
    public interface IBookSharingDbContext
    {
        DbSet<BookCoverImage> BookCoverImages { get; set; }

        DbSet<ProfileImage> ProfileImages { get; set; }

        DbSet<Book> Books { get; set; }

        DbSet<Author> Authors { get; set; }

        DbSet<Genre> Genres { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}

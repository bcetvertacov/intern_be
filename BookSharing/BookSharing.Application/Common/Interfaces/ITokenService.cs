﻿using System.Threading.Tasks;
using BookSharing.Application.Common.Models.Authentication;

namespace BookSharing.Application.Common.Interfaces
{
    public interface ITokenService
    {
        Task<TokenResponse> Authenticate(TokenRequest request);
    }
}
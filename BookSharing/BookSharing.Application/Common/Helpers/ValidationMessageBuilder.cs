﻿using System;

namespace BookSharing.Application.Common.Helpers
{
    /// <summary>
    /// Each validation message starts with property's name
    /// and ends with a period.
    /// </summary>
    internal static class ValidationMessageBuilder
    {
        public static string MustBeUnique(string propertyName)
        {
            return $"'{propertyName}' must be unique.";
        }

        public static string UndefinedEnum(string enumName)
        {
            return $"'{enumName}' must be a defined enum value.";
        }

        public static string EmptyCollection(string collectionName)
        {
            return $"'{collectionName}' does not contain any elements.";
        }

        public static string NonDistinctCollectionItems(string collectionName)
        {
            return $"'{collectionName}' contains repetitive items.";
        }

        public static string InvalidFileFormat(string objectName, string[] acceptedFileFormats = null)
        {
            string message = $"'{objectName}' invalid file format.";

            if (acceptedFileFormats != null)
            {
                message += $" Accepted formats: {string.Join(", ", acceptedFileFormats)}";
            }

            return message;
        }
    }
}

﻿using System;
using System.IO;
using Ardalis.GuardClauses;
using Microsoft.AspNetCore.Http;

namespace BookSharing.Application.Common.Utils
{
    public static class FormFileExtensions
    {
        public static byte[] ToByteArray(this IFormFile file)
        {
            Guard.Against.Null(file, nameof(file));

            using var binaryReader = new BinaryReader(file.OpenReadStream());
            var imageData = binaryReader.ReadBytes((int)file.Length);

            return imageData;
        }
    }
}

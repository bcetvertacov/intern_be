﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BookSharing.Application.Common.Models.Common;

namespace BookSharing.Application.Common.Utils.Query
{
    public interface IQueryBuilder
    {
        Task<QueryResult<TResult>> GetQueryResult<TSource, TResult>(
            IQueryable<TSource> dataSet,
            QueryOptions queryOptions,
            CancellationToken cancellationToken);
    }
}
﻿using Swashbuckle.AspNetCore.Annotations;

namespace BookSharing.Application.Common.Utils.Query
{
    public class QueryOptions
    {
        public QueryOptions()
        {
            // todo May be need set top by default not to return too much Top = 100;
            Skip = 0;
            //// Commented out for demo because not implemented - do not delete this comment
            ////Sort = new List<SortOptions>();
        }

        //// Commented out for demo because not implemented - do not delete this comment
        ///// <summary>
        ///// Gets sort options.
        ///// </summary>
        ////public IList<SortOptions> Sort { get; }

        [SwaggerParameter("Gets or sets number of Items per page. (e.g. top=20)")]
        public int Top { get; set; }

        [SwaggerParameter("Gets or sets number of Items to skip. (e.g. skip=40 for 3rd page to show).")]
        public int Skip { get; set; }
    }
}

﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BookSharing.Application.Common.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.Common.Utils.Query
{
    public class QueryBuilder : IQueryBuilder
    {
        private readonly IMapper _mapper;

        public QueryBuilder(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<QueryResult<TResult>> GetQueryResult<TSource, TResult>(
            IQueryable<TSource> dataSet,
            QueryOptions queryOptions,
            CancellationToken cancellationToken)
        {
            var query = dataSet;

            int totalCount = 0;

            if (queryOptions != null)
            {
                // todo Apply Filter
                // todo Apply Sorting
                if (queryOptions.Top > 0)
                {
                    totalCount = await query.CountAsync(cancellationToken);
                    query = ApplyPagination(query, queryOptions);
                }
            }

            var resultDataSet = await query
                .ProjectTo<TResult>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return new QueryResult<TResult>(resultDataSet, totalCount);
        }

        private static IQueryable<TSource> ApplyPagination<TSource>(
            IQueryable<TSource> query,
            QueryOptions queryOptions)
        {
            if (queryOptions.Skip > 0)
            {
               query = query.Skip(queryOptions.Skip);
            }

            // TODO: may be Validate Top. Math.Min(top, 100);
            query = query.Take(queryOptions.Top);

            return query;
        }
    }
}
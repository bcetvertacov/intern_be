﻿namespace BookSharing.Application.Common.Utils.Query
{
    public class SortOptions
    {
        public string FieldName { get; set; }

        public SortDirection SortDirection { get; set; }
    }
}

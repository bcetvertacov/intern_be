﻿namespace BookSharing.Application.Common.Utils.Query
{
    public enum SortDirection
    {
        /// <summary>
        /// Ascending sort direction
        /// </summary>
        Ascending,

        /// <summary>
        /// Descending sort direction
        /// </summary>
        Descending,
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace BookSharing.Application.Common.Behaviors
{
    // This behaviour validates each command  / query before proceeding to handling it.
    // Currently, FluentValidation is applied by default only if the command or query
    // is passed as an UI model (it is being passed as input parameter to our controller actions).
    public class ValidationBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationBehaviour(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(
            TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            Guard.Against.Null(next, nameof(next));

            if (_validators.Any())
            {
                var context = new ValidationContext<TRequest>(request);

                List<ValidationFailure> failures = new List<ValidationFailure>();

                foreach (var validator in _validators)
                {
                    var result = await validator.ValidateAsync(context);

                    if (result.Errors.Any())
                    {
                        failures.AddRange(result.Errors);
                    }
                }

                if (failures.Count != 0)
                {
                    throw new ValidationException(failures);
                }
            }

            return await next.Invoke();
        }
    }
}

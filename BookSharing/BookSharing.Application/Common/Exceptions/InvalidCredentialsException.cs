﻿using System;
using System.Runtime.Serialization;

namespace BookSharing.Application.Common.Exceptions
{
    /// <summary>
    /// Thrown when authentication didn't succeed due to invalid credentials.
    /// </summary>
    public class InvalidCredentialsException : Exception
    {
        public InvalidCredentialsException()
        {
        }

        public InvalidCredentialsException(string message)
            : base(message)
        {
        }

        public InvalidCredentialsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected InvalidCredentialsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

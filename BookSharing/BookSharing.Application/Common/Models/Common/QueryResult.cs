﻿using System.Collections.Generic;

namespace BookSharing.Application.Common.Models.Common
{
    public class QueryResult<T>
    {
        public QueryResult(IEnumerable<T> items, int totalCount)
        {
            Items = new List<T>(items);
            TotalCount = totalCount;
        }

        public IList<T> Items { get; }

        public int TotalCount { get; }
    }
}

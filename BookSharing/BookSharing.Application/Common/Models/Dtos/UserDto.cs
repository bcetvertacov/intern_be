﻿using BookSharing.Domain.Entities;

namespace BookSharing.Application.Common.Models.Dtos
{
    public class UserDto
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public ProfileImage ProfileImage { get; set; }
    }
}

﻿namespace BookSharing.Application.Common.Models.Authentication
{
    public class Token
    {
        public string Secret { get; set; }

        public string Audience { get; set; }

        public string Issuer { get; set; }

        public int ExpirationTime { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;

namespace BookSharing.Application.Common.Models.ViewModels
{
    public class ProfileImageViewModelIn
    {
        public IFormFile Image { get; set; }
    }
}

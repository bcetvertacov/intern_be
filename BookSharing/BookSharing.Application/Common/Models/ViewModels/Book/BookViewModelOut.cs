﻿using System;
using System.Collections.Generic;
using BookSharing.Domain.Enums;
using Swashbuckle.AspNetCore.Annotations;

namespace BookSharing.Application.Common.Models.ViewModels.Book
{
    public class BookViewModelOut
    {
        [SwaggerSchema("The book identifier")]
        public Guid Id { get; set; }

        public string CoverImage { get; set; }

        public string Title { get; set; }

        public virtual ICollection<AuthorViewModelOut> Authors { get; private set; } = new List<AuthorViewModelOut>();

        public Language Language { get; set; }

        public virtual ICollection<GenreViewModelOut> Genres { get; private set; } = new List<GenreViewModelOut>();

        public string OwnerId { get; set; }

        public DateTime PublicationDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using BookSharing.Domain.Enums;
using Microsoft.AspNetCore.Http;

namespace BookSharing.Application.Common.Models.ViewModels.Book
{
    public class BookViewModelIn
    {
        public IFormFile CoverImage { get; set; }

        public string Title { get; set; }

        public IEnumerable<Guid> Genres { get; set; }

        public Language Language { get; set; }

        public DateTime PublicationDate { get; set; }

        // IDs of existing authors
        public IEnumerable<Guid> Authors { get; set; }

        // Name of new authors
        // --------------------
        // Probably in the future this property will get removed and the logic
        // of creating new authors will be handled by some other controller, so
        // Frontend will first send a request to create new authors, then the ids
        // of newly created authors will be inserted into the list of author ids.
        // We will leave it like this for the sake of simplicity.
        public IEnumerable<AuthorViewModelIn> NewAuthors { get; set; }
    }
}

﻿using System;

namespace BookSharing.Application.Common.Models.ViewModels.Book
{
    public class GenreViewModelOut
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}

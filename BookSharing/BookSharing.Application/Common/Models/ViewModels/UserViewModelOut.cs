﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookSharing.Domain.Entities;

namespace BookSharing.Application.Common.Models.ViewModels
{
    public class UserViewModelOut
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string ProfileImage { get; set; }
    }
}

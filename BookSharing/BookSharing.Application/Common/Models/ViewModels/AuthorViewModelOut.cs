﻿using System;

namespace BookSharing.Application.Common.Models.ViewModels
{
    public class AuthorViewModelOut
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsDraft { get; set; }

        public string AddedByUserId { get; set; }
    }
}

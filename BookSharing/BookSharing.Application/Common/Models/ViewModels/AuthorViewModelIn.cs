﻿namespace BookSharing.Application.Common.Models.ViewModels
{
    public class AuthorViewModelIn
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}

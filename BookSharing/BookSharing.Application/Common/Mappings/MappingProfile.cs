﻿using System;
using AutoMapper;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils;
using BookSharing.Domain.Entities;

namespace BookSharing.Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // CreateMap<Guid, Guid>();
            CreateMap<Genre, GenreViewModelOut>();

            CreateMap<AuthorViewModelIn, Author>();
            CreateMap<Author, AuthorViewModelOut>();
        }
    }
}
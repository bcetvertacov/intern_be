﻿using System;
using AutoMapper;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Domain.Entities;

namespace BookSharing.Application.Common.Mappings
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<BookViewModelIn, Book>()
                .ForMember(
                    dest => dest.CoverImage,
                    opt => opt.Ignore())
                .ForMember(
                    dest => dest.Authors,
                    opt => opt.Ignore())
                .ForMember(
                    dest => dest.Genres,
                    opt => opt.Ignore());

            CreateMap<Book, BookViewModelOut>()
                .ForMember(
                    dest => dest.CoverImage,
                    opt => opt.MapFrom(src =>
                        src.CoverImage.Image == null
                            ? null
                            : Convert.ToBase64String(src.CoverImage.Image)));
        }
    }
}

﻿using System;
using AutoMapper;
using BookSharing.Application.Common.Models.Dtos;
using BookSharing.Application.Common.Models.ViewModels;

namespace BookSharing.Application.Common.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, UserViewModelOut>()
                .ForMember(
                    dest => dest.ProfileImage,
                    opt => opt.MapFrom(
                    src => src.ProfileImage != null && src.ProfileImage.Image != null
                        ? Convert.ToBase64String(src.ProfileImage.Image)
                        : null));
        }
    }
}

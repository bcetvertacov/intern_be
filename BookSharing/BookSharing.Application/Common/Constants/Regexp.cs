﻿namespace BookSharing.Application.Common.Constants
{
    public static class Regexp
    {
        public const string Email = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string Alphabetic = @"^\p{L}+$";
        public const string Alphanumeric = @"^[\p{L}\p{N}]+$";
    }
}

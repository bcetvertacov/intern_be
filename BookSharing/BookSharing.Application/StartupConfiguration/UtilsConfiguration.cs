﻿using BookSharing.Application.Common.Utils.Query;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Application.StartupConfiguration
{
    public static class UtilsConfiguration
    {
        public static IServiceCollection ConfigureUtils(this IServiceCollection services)
        {
            services.AddScoped<IQueryBuilder, QueryBuilder>();

            return services;
        }
    }
}

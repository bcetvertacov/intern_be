﻿using System.Reflection;
using BookSharing.Application.Common.Behaviors;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace BookSharing.Application.StartupConfiguration
{
    public static class MediatorConfiguration
    {
        public static IServiceCollection ConfigureMediator(this IServiceCollection services)
        {
            // Using the AddMediatR command, we specify the assembly that
            // where Mediator will search for Commands and coresponding CommandHandlers.
            services.AddMediatR(Assembly.GetExecutingAssembly());

            // MediatR pipelines allow us to specify some actions to be performed either before
            // or ather the actual command handling.
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));

            return services;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Application.Common.Utils;
using BookSharing.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.CQRS.BookActions.Commands.CreateBook
{
    // NEXTSPRINT: Move data manipulation logic to some sort of book repository.
    public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, Guid>
    {
        private readonly IBookSharingDbContext _dbContext;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public CreateBookCommandHandler(
            IBookSharingDbContext dbContext,
            ICurrentUserService currentUserService,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            var bookVm = request.Book;

            var book = _mapper.Map<Book>(bookVm);
            book.OwnerId = _currentUserService.GetUserId();

            if (bookVm.CoverImage != null)
            {
                SetCoverImage(book, bookVm.CoverImage);
            }

            if (bookVm.Authors != null)
            {
                await AssignAuthors(book, bookVm.Authors);
            }

            if (bookVm.Genres != null)
            {
                await AssignGenres(book, bookVm.Genres);
            }

            if (bookVm.NewAuthors != null)
            {
                AssignNewAuthors(book, bookVm.NewAuthors);
            }

            await _dbContext.Books.AddAsync(book, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return book.Id;
        }

        private static void SetCoverImage(Book book, IFormFile coverImage)
        {
            book.CoverImage = new BookCoverImage()
            {
               Image = coverImage?.ToByteArray()
            };
        }

        private void AssignNewAuthors(Book book, IEnumerable<AuthorViewModelIn> newAuthors)
        {
            foreach (var author in newAuthors)
            {
                var authorModel = _mapper.Map<Author>(author);

                authorModel.IsDraft = true;
                authorModel.AddedByUserId = _currentUserService.GetUserId();

                book.Authors.Add(authorModel);
            }
        }

        private async Task AssignAuthors(Book book, IEnumerable<Guid> authorIds)
        {
            await _dbContext.Authors
                .Where(a => authorIds.Any(id => a.Id == id))
                .ForEachAsync(a => book.Authors.Add(a));
        }

        private async Task AssignGenres(Book book, IEnumerable<Guid> genreIds)
        {
            await _dbContext.Genres
                .Where(g => genreIds.Any(id => g.Id == id))
                .ForEachAsync(g => book.Genres.Add(g));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BookSharing.Application.Common.Helpers;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Domain.Enums;
using FluentValidation;

namespace BookSharing.Application.CQRS.BookActions.Commands.CreateBook
{
    public class CreateBookCommandValidator : AbstractValidator<CreateBookCommand>
    {
        private readonly string[] _allowedExtensions = { ".png", ".jpg" };

        public CreateBookCommandValidator()
        {
            RuleFor(b => b.Book.CoverImage)
               .Must(i => HaveValidFileFormat(i.FileName))
               .WithMessage(b => ValidationMessageBuilder.InvalidFileFormat(nameof(b.Book.CoverImage), _allowedExtensions))
               .When(b => b.Book.CoverImage != null);

            RuleFor(b => b.Book.Title)
                .Length(3, 250)
                .NotEmpty();

            RuleFor(b => b.Book.Genres)
                .NotEmpty();

            RuleFor(b => b.Book.Genres)
                .Must(ContainDistinctItems)
                .WithMessage(b => ValidationMessageBuilder.NonDistinctCollectionItems(nameof(b.Book.Genres)));

            RuleFor(b => b.Book.Language)
                .Must(l => Enum.IsDefined(typeof(Language), l))
                .WithMessage(b => ValidationMessageBuilder.UndefinedEnum(nameof(b.Book.Language)));

            RuleFor(b => b.Book.PublicationDate)
                .NotNull();

            RuleFor(b => b.Book)
                .Must(HaveAuthors)
                .WithMessage(b => ValidationMessageBuilder.EmptyCollection(nameof(b.Book.Authors)));

            RuleFor(b => b.Book.Authors)
               .Must(ContainDistinctItems)
               .WithMessage(b => ValidationMessageBuilder.NonDistinctCollectionItems(nameof(b.Book.Authors)));

            RuleFor(b => b.Book.NewAuthors)
               .Must(ContainDistinctItems)
               .WithMessage(b => ValidationMessageBuilder.NonDistinctCollectionItems(nameof(b.Book.NewAuthors)));
        }

        private static bool ContainDistinctItems<T>(IEnumerable<T> collection)
        {
            if (collection == null)
            {
                return true;
            }

            return collection.Distinct().Count() == collection.Count();
        }

        private bool HaveAuthors(BookViewModelIn book)
        {
            int totalAuthors = (book.Authors?.Count() ?? 0) + (book.NewAuthors?.Count() ?? 0);
            return totalAuthors > 0;
        }

        private bool HaveValidFileFormat(string fileName)
        {
            return _allowedExtensions.Contains(Path.GetExtension(fileName));
        }
    }
}

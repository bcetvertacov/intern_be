﻿using System;
using BookSharing.Application.Common.Models.ViewModels.Book;
using MediatR;

namespace BookSharing.Application.CQRS.BookActions.Commands.CreateBook
{
    public class CreateBookCommand : IRequest<Guid>
    {
        public BookViewModelIn Book { get; set; }
    }
}

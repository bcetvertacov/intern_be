﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace BookSharing.Application.CQRS.BookActions.Queries
{
    public class GetAllBooksQueryHandler : IRequestHandler<GetAllBooksQuery, QueryResult<BookViewModelOut>>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;

        private readonly IQueryBuilder _queryBuilder;

        public GetAllBooksQueryHandler(
            IBookSharingDbContext bookSharingDbContext,
            IQueryBuilder queryBuilder)
        {
            _bookSharingDbContext = bookSharingDbContext;

            _queryBuilder = queryBuilder;
        }

        public async Task<QueryResult<BookViewModelOut>> Handle(GetAllBooksQuery request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            return await _queryBuilder.GetQueryResult<Book, BookViewModelOut>(
                _bookSharingDbContext.Books.Where(b => !b.Authors.Any(a => a.IsDraft)),
                request.QueryOptions,
                cancellationToken);
        }
    }
}

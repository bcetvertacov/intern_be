﻿using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using MediatR;

namespace BookSharing.Application.CQRS.BookActions.Queries
{
    public class GetAllBooksQuery : IRequest<QueryResult<BookViewModelOut>>
    {
        public QueryOptions QueryOptions { get; set; }
    }
}

﻿using System.Collections.Generic;
using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.AuthorActions.Queries
{
    public class GetAllAuthorsQuery : IRequest<IEnumerable<AuthorViewModelOut>>
    {
    }
}

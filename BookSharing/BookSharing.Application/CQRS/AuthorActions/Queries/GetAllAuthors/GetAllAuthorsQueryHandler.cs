﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.CQRS.AuthorActions.Queries
{
    public class GetAllAuthorsQueryHandler : IRequestHandler<GetAllAuthorsQuery, IEnumerable<AuthorViewModelOut>>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public GetAllAuthorsQueryHandler(
            IBookSharingDbContext bookSharingDbContext,
            IMapper mapper,
            ICurrentUserService currentUserService)
        {
            _bookSharingDbContext = bookSharingDbContext;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }

        public async Task<IEnumerable<AuthorViewModelOut>> Handle(GetAllAuthorsQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var userId = _currentUserService.GetUserId();

            // At the moment this condition remains as it is
            // in next task INTPO002PD-36 will be implemented filter options and will move these parameters there
            var allAuthors = await _bookSharingDbContext.Authors.Where(a => !a.IsDraft || a.AddedByUserId == userId)
                .ProjectTo<AuthorViewModelOut>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return allAuthors;
        }
    }
}

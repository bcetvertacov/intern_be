﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.ViewModels;
using BookSharing.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.CQRS.AuthorActions.Queries
{
    public class GetAuthorByIdQueryHandler : IRequestHandler<GetAuthorByIdQuery, AuthorViewModelOut>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;

        private readonly IMapper _mapper;

        public GetAuthorByIdQueryHandler(
            IBookSharingDbContext bookSharingDbContext,
            IMapper mapper)
        {
            _bookSharingDbContext = bookSharingDbContext;
            _mapper = mapper;
        }

        public async Task<AuthorViewModelOut> Handle(GetAuthorByIdQuery request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            var author = await _bookSharingDbContext.Authors
                .Where(a => a.Id == request.Id)
                .ProjectTo<AuthorViewModelOut>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);

            if (author == null)
            {
                throw new Common.Exceptions.NotFoundException(typeof(Author).Name, request.Id);
            }

            return author;
        }
    }
}

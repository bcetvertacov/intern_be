﻿using System;
using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.AuthorActions.Queries
{
    public class GetAuthorByIdQuery : IRequest<AuthorViewModelOut>
    {
        public Guid Id { get; set; }
    }
}

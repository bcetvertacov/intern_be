﻿using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.AuthorActions.Commands
{
    public class CreateAuthorCommand : AuthorViewModelIn, IRequest<string>
    {
    }
}

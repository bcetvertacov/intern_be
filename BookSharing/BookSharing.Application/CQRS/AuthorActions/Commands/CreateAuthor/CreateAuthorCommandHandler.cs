﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Domain.Entities;
using MediatR;

namespace BookSharing.Application.CQRS.AuthorActions.Commands
{
    public class CreateAuthorCommandHandler : IRequestHandler<CreateAuthorCommand, string>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public CreateAuthorCommandHandler(
            IBookSharingDbContext bookSharingDbContext,
            IMapper mapper,
            ICurrentUserService currentUserService)
        {
            _bookSharingDbContext = bookSharingDbContext;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }

        public async Task<string> Handle(CreateAuthorCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var author = _mapper.Map<Author>(request);
            var userId = _currentUserService.GetUserId();

            author.IsDraft = true;
            author.AddedByUserId = _currentUserService.GetUserId();

            await _bookSharingDbContext.Authors.AddAsync(author, cancellationToken);
            await _bookSharingDbContext.SaveChangesAsync(cancellationToken);
            return author.Id.ToString();
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using BookSharing.Application.Common.Helpers;
using BookSharing.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace BookSharing.Application.CQRS.AuthorActions.Commands.CreateAuthor
{
    public class CreateAuthorCommandValidator : AbstractValidator<CreateAuthorCommand>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;

        public CreateAuthorCommandValidator(IBookSharingDbContext bookSharingDbContext)
        {
            _bookSharingDbContext = bookSharingDbContext;

            RuleFor(a => a.FirstName)
                .MaximumLength(100);

            RuleFor(a => a.LastName)
                .Length(3, 100);

            RuleFor(a => a.FirstName + a.LastName)
                .MustAsync(BeUniqueAuthorName)
                .WithMessage(a => ValidationMessageBuilder.MustBeUnique("The combination of First and Last name"));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "Sql can't understand ToUpper(CiltureInfo)")]
        private async Task<bool> BeUniqueAuthorName(string authorName, CancellationToken token)
        {
            return !(await _bookSharingDbContext.Authors.AnyAsync(
                a => authorName.ToUpper() == a.FirstName.ToUpper() + a.LastName.ToUpper(),
                token));
        }
    }
}

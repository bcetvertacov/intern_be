﻿using BookSharing.Application.Common.Constants;
using FluentValidation;

namespace BookSharing.Application.CQRS.AuthActions.Commands.Login
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(x => x.Username)
                .Length(3, 30)
                .Matches(Regexp.Alphanumeric);

            RuleFor(u => u.Password)
                .Length(3, 30)
                .Matches(Regexp.Alphanumeric);
        }
    }
}
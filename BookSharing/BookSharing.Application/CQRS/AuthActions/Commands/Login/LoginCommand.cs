﻿using BookSharing.Application.Common.Models.Authentication;
using MediatR;

namespace BookSharing.Application.CQRS.AuthActions.Commands.Login
{
    public class LoginCommand : TokenRequest, IRequest<TokenResponse>
    {
    }
}

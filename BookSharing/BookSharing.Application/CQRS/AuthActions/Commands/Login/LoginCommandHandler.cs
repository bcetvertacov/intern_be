﻿using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Authentication;
using MediatR;

namespace BookSharing.Application.CQRS.AuthActions.Commands.Login
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, TokenResponse>
    {
        private readonly ITokenService _tokenService;

        public LoginCommandHandler(ITokenService tokenService)
        {
            Guard.Against.Null(tokenService, nameof(tokenService));

            _tokenService = tokenService;
        }

        public async Task<TokenResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            TokenResponse tokenResponse = await _tokenService.Authenticate(request);

            return tokenResponse;
        }
    }
}

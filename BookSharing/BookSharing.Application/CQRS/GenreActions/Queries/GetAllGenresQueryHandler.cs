﻿using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Domain.Entities;
using MediatR;

namespace BookSharing.Application.CQRS.GenreActions.Queries
{
    public class GetAllGenresQueryHandler : IRequestHandler<GetAllGenresQuery, QueryResult<GenreViewModelOut>>
    {
        private readonly IBookSharingDbContext _bookSharingDbContext;

        private readonly IQueryBuilder _queryBuilder;

        public GetAllGenresQueryHandler(
           IBookSharingDbContext bookSharingDbContext,
           IQueryBuilder queryBuilder)
        {
            _bookSharingDbContext = bookSharingDbContext;

            _queryBuilder = queryBuilder;
        }

        public async Task<QueryResult<GenreViewModelOut>> Handle(GetAllGenresQuery request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            return await _queryBuilder.GetQueryResult<Genre, GenreViewModelOut>(
                _bookSharingDbContext.Genres,
                request.QueryOptions,
                cancellationToken);
        }
    }
}

﻿using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using MediatR;

namespace BookSharing.Application.CQRS.GenreActions.Queries
{
    public class GetAllGenresQuery : IRequest<QueryResult<GenreViewModelOut>>
    {
        public QueryOptions QueryOptions { get; set; }
    }
}

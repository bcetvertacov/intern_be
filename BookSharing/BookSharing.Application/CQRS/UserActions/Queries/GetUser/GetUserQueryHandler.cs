﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using BookSharing.Application.Common.Exceptions;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Dtos;
using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Queries.GetUser
{
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserViewModelOut>
    {
        private readonly IIdentityService _identityService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public GetUserQueryHandler(
            IIdentityService identityService,
            ICurrentUserService currentUserService,
            IMapper mapper)
        {
            _identityService = identityService;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public async Task<UserViewModelOut> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            if (request.UserId == null)
            {
                request.UserId = _currentUserService.GetUserId();
            }

            var user = await _identityService
                .GetUserByIdAsync(request.UserId);

            if (user is null)
            {
                throw new Common.Exceptions.NotFoundException(nameof(UserDto), request.UserId);
            }

            return _mapper.Map<UserViewModelOut>(user);
        }
    }
}

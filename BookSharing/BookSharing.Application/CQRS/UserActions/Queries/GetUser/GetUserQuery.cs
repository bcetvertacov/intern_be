﻿using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserViewModelOut>
    {
        public string UserId { get; set; }
    }
}

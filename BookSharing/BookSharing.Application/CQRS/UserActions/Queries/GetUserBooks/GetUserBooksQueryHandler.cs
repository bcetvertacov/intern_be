﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using AutoMapper;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using BookSharing.Domain.Entities;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Queries.GetUserBooks
{
    public class GetUserBooksQueryHandler : IRequestHandler<GetUserBooksQuery, QueryResult<BookViewModelOut>>
    {
        private readonly IBookSharingDbContext _dbContext;
        private readonly IQueryBuilder _queryBuilder;

        public GetUserBooksQueryHandler(
            IBookSharingDbContext dbContext,
            IQueryBuilder queryBuilder)
        {
            _dbContext = dbContext;
            _queryBuilder = queryBuilder;
        }

        public async Task<QueryResult<BookViewModelOut>> Handle(GetUserBooksQuery request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            var books = await _queryBuilder.GetQueryResult<Book, BookViewModelOut>(
                    _dbContext.Books.Where(b => b.OwnerId == request.UserId),
                    request.QueryOptions,
                    cancellationToken);

            return books;
        }
    }
}

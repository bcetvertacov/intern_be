﻿using System.Collections.Generic;
using BookSharing.Application.Common.Models.Common;
using BookSharing.Application.Common.Models.ViewModels.Book;
using BookSharing.Application.Common.Utils.Query;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Queries.GetUserBooks
{
    public class GetUserBooksQuery : IRequest<QueryResult<BookViewModelOut>>
    {
        public string UserId { get; set; }

        public QueryOptions QueryOptions { get; set; }
    }
}

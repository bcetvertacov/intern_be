﻿using System.Threading;
using System.Threading.Tasks;
using BookSharing.Application.Common.Interfaces;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Commands.CreateUser
{
    // We define the command handler this way:
    // It implements the IRequestHandler interface, which takes 1 or 2 type parameters:
    // TRequest (Command Type) and TResponse (if the command has a return type).
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, string>
    {
        // Inject only those services that are needed for this specific task:
        // Update an user.
        private readonly IIdentityService _identityService;

        public CreateUserCommandHandler(
            IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<string> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var userId = await _identityService.CreateUserAsync(request);

            return userId;
        }
    }
}

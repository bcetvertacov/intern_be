﻿using System.Threading;
using System.Threading.Tasks;
using BookSharing.Application.Common.Constants;
using BookSharing.Application.Common.Helpers;
using BookSharing.Application.Common.Interfaces;
using FluentValidation;

namespace BookSharing.Application.CQRS.UserActions.Commands.CreateUser
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        private readonly IIdentityService _identityService;

        public CreateUserCommandValidator(IIdentityService identityService)
        {
            _identityService = identityService;

            // TODO: Alphanumeric = is at least one digit required?
            // TODO: Add support for Unicode letters? Or only a-zA-Z
            RuleFor(u => u.FirstName)
                .Length(3, 30)
                .Matches(Regexp.Alphabetic);

            RuleFor(u => u.LastName)
                .Length(3, 30)
                .Matches(Regexp.Alphabetic);

            RuleFor(u => u.Username)
                .Length(3, 30)
                .Matches(Regexp.Alphanumeric);

            RuleFor(u => u.Username)
                .MustAsync(BeUniqueUsername)
                .WithMessage(u => ValidationMessageBuilder.MustBeUnique(nameof(u.Username)));

            RuleFor(u => u.Email)
                .Matches(Regexp.Email)
                .MaximumLength(70)
                .NotEmpty();

            RuleFor(u => u.Email)
                .MustAsync(BeUniqueEmail)
                .WithMessage(u => ValidationMessageBuilder.MustBeUnique(nameof(u.Email)));

            RuleFor(u => u.Password)
                .Length(3, 30)
                .Matches(Regexp.Alphanumeric);
        }

        private async Task<bool> BeUniqueEmail(string email, CancellationToken token)
        {
            return await _identityService.IsUniqueEmailAsync(email);
        }

        private async Task<bool> BeUniqueUsername(string username, CancellationToken token)
        {
            return await _identityService.IsUniqueUsernameAsync(username);
        }
    }
}

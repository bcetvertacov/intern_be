﻿using BookSharing.Application.Common.Models.ViewModels;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Commands.CreateUser
{
    // Each Command implements the IRequest<ReturnType> interface.
    // It contains data necessary for the handler to handle the request.
    // In this case, we need FirstName, LastName, UserName, Email, Password.
    // Those are in UserViewModelIn
    public class CreateUserCommand : UserViewModelIn, IRequest<string>
    {
    }
}

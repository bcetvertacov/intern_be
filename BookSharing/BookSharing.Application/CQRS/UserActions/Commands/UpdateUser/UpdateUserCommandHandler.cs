﻿using System.Threading;
using System.Threading.Tasks;
using Ardalis.GuardClauses;
using BookSharing.Application.Common.Interfaces;
using BookSharing.Application.Common.Utils;
using MediatR;

namespace BookSharing.Application.CQRS.UserActions.Commands.UpdateUser
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
    {
        private readonly IIdentityService _identityService;
        private readonly ICurrentUserService _currentUserService;

        public UpdateUserCommandHandler(
            IIdentityService identityService,
            ICurrentUserService currentUserService)
        {
            _identityService = identityService;
            _currentUserService = currentUserService;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            Guard.Against.Null(request, nameof(request));

            if (request.UserId is null)
            {
                request.UserId = _currentUserService.GetUserId();
            }

            // For now, according to current user stories, only profile image
            // is editable
            await _identityService.SetProfileImage(
                request.UserId,
                request.ProfileImage.Image.ToByteArray());

            return Unit.Value;
        }
    }
}

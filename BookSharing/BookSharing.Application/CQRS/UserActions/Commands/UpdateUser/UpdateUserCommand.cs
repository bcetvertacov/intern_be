﻿using BookSharing.Application.Common.Models.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace BookSharing.Application.CQRS.UserActions.Commands.UpdateUser
{
    public class UpdateUserCommand : IRequest
    {
        // If userId is null, then the current user will be updated.
        public string UserId { get; set; }

        public ProfileImageViewModelIn ProfileImage { get; set; }
    }
}

﻿using System.IO;
using System.Linq;
using FluentValidation;

namespace BookSharing.Application.CQRS.UserActions.Commands.UpdateUser
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        private readonly string[] _allowedExtensions = { ".png", ".jpg" };

        public UpdateUserCommandValidator()
        {
            RuleFor(u => u.ProfileImage.Image)
                .NotEmpty();

            RuleFor(u => Path.GetExtension(u.ProfileImage.Image.FileName))
                .Must(extension => _allowedExtensions.Contains(extension))
                .WithMessage($"File format should be one of \"{string.Join(", ", _allowedExtensions)}\"")
                .When(u => u.ProfileImage.Image != null);

            RuleFor(u => u.ProfileImage.Image.Length)
                .LessThan(10 * 1024 * 1024)
                .WithMessage("Image size can not be more that 10 MB")
                .When(u => u.ProfileImage.Image != null);
        }
    }
}
